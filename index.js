console.log("Hello World!");

// Exponent Operator and Template literals
let getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);

// Destructure the array
let address = [ "Block 46-A", "Pampano St.", "Brgy. Longos", "Malabon", "Metro Manila", 1472 ];

let [ houseNumber, street, barangay, municipality, Province, zipCode ] = address;
console.log(`I live at ${houseNumber} ${street} ${barangay}, ${municipality}, ${Province} ${zipCode}`);

// Destructure the object
let animal= { 
	name : "Lolong",
	species :"Salwater crocodile",
	weight : "1075 kgs",
	measurement : "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;

console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}`);

// Arrow Function in forEach and reduce 
let arrNum = [1, 2, 3, 4, 5];

arrNum.forEach((num) =>{
	console.log(num);
}); 

let reduceNumber = arrNum.reduce((x, y) => x+y);
console.log(reduceNumber);

//Dog class 
class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let janna = new Dog("Janna", 5, "Aspin");
console.log(janna);
